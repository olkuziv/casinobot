﻿using System;
using System.Windows.Forms;
using Autofac;
using CasinoBot.Configs;
using CasinoBot.Views;

namespace CasinoBot
{
    static class Program
    {
        
        [STAThread]
        static void Main()
        {
            var container = AutofacConfig.Configure();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(container.Resolve<Start>());
        }
    }
}
