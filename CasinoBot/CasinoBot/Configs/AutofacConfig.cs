﻿using Autofac;
using CasinoBot.Abstractions;
using CasinoBot.Models;
using CasinoBot.Views;
using CasinoBot.Loggers;

namespace CasinoBot.Configs
{
    static class AutofacConfig
    {
        private static IContainer s_containerBuilder;

        public static IContainer ContainerBuilder {
            get {
                if (s_containerBuilder == null)
                    s_containerBuilder = Configure();

                return s_containerBuilder;
            }
            private set => s_containerBuilder = value;
        }

        public static IContainer Configure()
        {
            var worker = new YobitWorker("ruskuziv@gmail.com", "Rk2504762936");
            var loggers = new ILogger[]
            {
                new ConsoleLogger(), new FileLogger(), new HistoryFileLogger()
            };
            var builder = new ContainerBuilder();

            builder
                .RegisterType<Start>()
                .AsSelf();

            builder
                .RegisterInstance(worker)
                .As<IWebWorker>();

            builder
                .RegisterType<LoggerManager>()
                .AsSelf();
            builder
                .RegisterInstance(loggers)
                .As<ILogger[]>();


            ContainerBuilder = builder.Build();
            return ContainerBuilder;
        }
    }
}
