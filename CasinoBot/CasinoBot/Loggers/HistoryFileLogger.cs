﻿using System.IO;
using CasinoBot.Abstractions;
using CasinoBot.Models;

namespace CasinoBot.Loggers
{
    public class HistoryFileLogger : ILogger
    {
        public static string S_Path { get; private set; } = "Logs/History.log";

        public HistoryFileLogger()
        {
            if (!Directory.Exists("Logs"))
                Directory.CreateDirectory("Logs");
        }

        public string Path { get=>S_Path; private set=>S_Path = value; } 

        public void Log(LogMessage message)
        {
            if (message.IsError) return;

            File.AppendAllText(Path, message.Message + "\n");
        }
    }
}
