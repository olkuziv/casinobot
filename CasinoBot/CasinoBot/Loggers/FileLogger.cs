﻿using System.IO;
using CasinoBot.Abstractions;
using CasinoBot.Models;

namespace CasinoBot.Loggers
{
    public class FileLogger : ILogger
    {
        public FileLogger()
        {
            if (!Directory.Exists("Logs"))
                Directory.CreateDirectory("Logs");
        }

        public string Path { get; private set; } = "Logs/Log.log";
        public void Log(LogMessage message)
        {
            if(message.IsError)
                File.AppendAllText(Path, message.Message + "\n");
        }
    }
}
