﻿using System;
using CasinoBot.Abstractions;
using CasinoBot.Models;

namespace CasinoBot.Loggers
{
    public class ConsoleLogger : ILogger
    {
        public string Path { get; } = null;

        public void Log(LogMessage message)
        {
            Console.WriteLine(message.Message);
        }
    }
}
