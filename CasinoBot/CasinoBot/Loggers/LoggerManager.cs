﻿using System;
using CasinoBot.Abstractions;
using CasinoBot.Models;

namespace CasinoBot.Loggers
{
    public class LoggerManager
    {
        private readonly ILogger[] _loggers;

        public LoggerManager(ILogger[] loggers)
        {
            _loggers = loggers;
        }

        public void Log(LogMessage message)
        {
            if(_loggers == null || _loggers.Length == 0)
                throw new Exception("Немає доступних логерів! Помилка Log1Error");

            foreach (var item in _loggers)
            {
                item.Log(message);
            }
        }

    }
}
