﻿namespace CasinoBot.Views
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OpenBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblWalet = new System.Windows.Forms.Label();
            this.btnAutoWork = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.lblStartCashValue = new System.Windows.Forms.Label();
            this.lblCoefficientValue = new System.Windows.Forms.Label();
            this.lblLoseCountValue = new System.Windows.Forms.Label();
            this.lblCoefficient = new System.Windows.Forms.Label();
            this.lblLoseCount = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxMaxLoseSum = new System.Windows.Forms.TextBox();
            this.tbxСoefficient = new System.Windows.Forms.TextBox();
            this.tbxMinBet = new System.Windows.Forms.TextBox();
            this.lblStartCash = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // OpenBtn
            // 
            this.OpenBtn.Location = new System.Drawing.Point(10, 294);
            this.OpenBtn.Name = "OpenBtn";
            this.OpenBtn.Size = new System.Drawing.Size(119, 24);
            this.OpenBtn.TabIndex = 0;
            this.OpenBtn.Text = "Відкрити браузер";
            this.OpenBtn.UseVisualStyleBackColor = true;
            this.OpenBtn.Click += new System.EventHandler(this.OpenBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Баланс:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblWalet
            // 
            this.lblWalet.AutoSize = true;
            this.lblWalet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWalet.Location = new System.Drawing.Point(84, 20);
            this.lblWalet.Name = "lblWalet";
            this.lblWalet.Size = new System.Drawing.Size(0, 20);
            this.lblWalet.TabIndex = 4;
            // 
            // btnAutoWork
            // 
            this.btnAutoWork.Location = new System.Drawing.Point(151, 294);
            this.btnAutoWork.Name = "btnAutoWork";
            this.btnAutoWork.Size = new System.Drawing.Size(119, 24);
            this.btnAutoWork.TabIndex = 5;
            this.btnAutoWork.Text = "Запустити";
            this.btnAutoWork.UseVisualStyleBackColor = true;
            this.btnAutoWork.Click += new System.EventHandler(this.btnAutoClick_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(840, 352);
            this.tabControl.TabIndex = 6;
            this.tabControl.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl_Selecting);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.lblStartCashValue);
            this.tabPage1.Controls.Add(this.lblCoefficientValue);
            this.tabPage1.Controls.Add(this.lblLoseCountValue);
            this.tabPage1.Controls.Add(this.lblCoefficient);
            this.tabPage1.Controls.Add(this.lblLoseCount);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.lblStartCash);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnAutoWork);
            this.tabPage1.Controls.Add(this.OpenBtn);
            this.tabPage1.Controls.Add(this.lblWalet);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(832, 326);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Налаштування бота";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listView1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox2.Location = new System.Drawing.Point(385, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(190, 320);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Розрахунок стовок";
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(3, 16);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(184, 301);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // lblStartCashValue
            // 
            this.lblStartCashValue.AutoSize = true;
            this.lblStartCashValue.Location = new System.Drawing.Point(148, 120);
            this.lblStartCashValue.Name = "lblStartCashValue";
            this.lblStartCashValue.Size = new System.Drawing.Size(13, 13);
            this.lblStartCashValue.TabIndex = 9;
            this.lblStartCashValue.Text = "0";
            // 
            // lblCoefficientValue
            // 
            this.lblCoefficientValue.AutoSize = true;
            this.lblCoefficientValue.Location = new System.Drawing.Point(148, 92);
            this.lblCoefficientValue.Name = "lblCoefficientValue";
            this.lblCoefficientValue.Size = new System.Drawing.Size(13, 13);
            this.lblCoefficientValue.TabIndex = 9;
            this.lblCoefficientValue.Text = "0";
            // 
            // lblLoseCountValue
            // 
            this.lblLoseCountValue.AutoSize = true;
            this.lblLoseCountValue.Location = new System.Drawing.Point(148, 64);
            this.lblLoseCountValue.Name = "lblLoseCountValue";
            this.lblLoseCountValue.Size = new System.Drawing.Size(13, 13);
            this.lblLoseCountValue.TabIndex = 9;
            this.lblLoseCountValue.Text = "0";
            // 
            // lblCoefficient
            // 
            this.lblCoefficient.AutoSize = true;
            this.lblCoefficient.Location = new System.Drawing.Point(10, 90);
            this.lblCoefficient.Name = "lblCoefficient";
            this.lblCoefficient.Size = new System.Drawing.Size(64, 13);
            this.lblCoefficient.TabIndex = 8;
            this.lblCoefficient.Text = "Коефіцієнт:";
            // 
            // lblLoseCount
            // 
            this.lblLoseCount.AutoSize = true;
            this.lblLoseCount.Location = new System.Drawing.Point(10, 64);
            this.lblLoseCount.Name = "lblLoseCount";
            this.lblLoseCount.Size = new System.Drawing.Size(113, 13);
            this.lblLoseCount.TabIndex = 7;
            this.lblLoseCount.Text = "Кількість програшів: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tbxMaxLoseSum);
            this.groupBox1.Controls.Add(this.tbxСoefficient);
            this.groupBox1.Controls.Add(this.tbxMinBet);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.groupBox1.Location = new System.Drawing.Point(575, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 320);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Налаштування бота";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Максимальна ставка";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Коефіцієнт";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Мінімальна ставка";
            // 
            // tbxMaxLoseSum
            // 
            this.tbxMaxLoseSum.Location = new System.Drawing.Point(7, 107);
            this.tbxMaxLoseSum.Name = "tbxMaxLoseSum";
            this.tbxMaxLoseSum.Size = new System.Drawing.Size(198, 21);
            this.tbxMaxLoseSum.TabIndex = 0;
            this.tbxMaxLoseSum.Text = "0";
            // 
            // tbxСoefficient
            // 
            this.tbxСoefficient.Location = new System.Drawing.Point(7, 156);
            this.tbxСoefficient.Name = "tbxСoefficient";
            this.tbxСoefficient.Size = new System.Drawing.Size(198, 21);
            this.tbxСoefficient.TabIndex = 0;
            this.tbxСoefficient.Text = "3";
            this.tbxСoefficient.Leave += new System.EventHandler(this.tbxСoefficient_Leave);
            // 
            // tbxMinBet
            // 
            this.tbxMinBet.Location = new System.Drawing.Point(7, 58);
            this.tbxMinBet.Name = "tbxMinBet";
            this.tbxMinBet.Size = new System.Drawing.Size(198, 21);
            this.tbxMinBet.TabIndex = 0;
            this.tbxMinBet.Text = "0.00000001";
            this.tbxMinBet.Leave += new System.EventHandler(this.tbxMinBet_Leave);
            // 
            // lblStartCash
            // 
            this.lblStartCash.AutoSize = true;
            this.lblStartCash.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.lblStartCash.Location = new System.Drawing.Point(10, 121);
            this.lblStartCash.Name = "lblStartCash";
            this.lblStartCash.Size = new System.Drawing.Size(131, 13);
            this.lblStartCash.TabIndex = 3;
            this.lblStartCash.Text = "Баланс до запуску бота:";
            this.lblStartCash.Click += new System.EventHandler(this.label1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(832, 326);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Переглянути звіт";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.column2});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(826, 320);
            this.dataGridView1.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Кількість програшів";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // column2
            // 
            this.column2.HeaderText = "Кількість повторень";
            this.column2.Name = "column2";
            this.column2.ReadOnly = true;
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 352);
            this.Controls.Add(this.tabControl);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "Start";
            this.Text = "Start";
            this.tabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OpenBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblWalet;
        private System.Windows.Forms.Button btnAutoWork;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxMaxLoseSum;
        private System.Windows.Forms.TextBox tbxСoefficient;
        private System.Windows.Forms.TextBox tbxMinBet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCoefficient;
        private System.Windows.Forms.Label lblLoseCount;
        private System.Windows.Forms.Label lblCoefficientValue;
        private System.Windows.Forms.Label lblLoseCountValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn column2;
        private System.Windows.Forms.Label lblStartCashValue;
        private System.Windows.Forms.Label lblStartCash;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView listView1;
    }
}