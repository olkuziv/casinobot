﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CasinoBot.Abstractions;
using CasinoBot.Loggers;
using CasinoBot.Models;
using Exception = System.Exception;

namespace CasinoBot.Views
{
    public partial class Start : Form
    {
        private (decimal sum, string type) _cash;
        private bool _isLeftButton;

        private LoggerManager Logger { get; }
        private IWebWorker Worker { get; }


        #region Properties

        private (Decimal sum, string type) Cash
        {
            get => _cash;
            set
            {
                _cash = value;
                lblWalet.Text = Cash.sum + " " + Cash.type;
            }
        }

        private decimal[] Coefficients => Worker.ParseCoefficients(tbxСoefficient.Text);

        private bool IsLeftButton
        {
            get => _isLeftButton;
            set
            {
                _isLeftButton = value;
                if (value) Worker.ClickLeftAsync();
                else Worker.ClickRightAsync();
            }
        }

        private bool RandomBool => new Random().Next(100) < 50;

        #endregion

        #region Constructor

        public Start(IWebWorker worker, LoggerManager logger)
        {
            InitializeComponent();

            Logger = logger;
            Worker = worker;
        }

        #endregion


        private async void OpenBtn_Click(object sender, EventArgs e)
        {
            try
            {
                await Task.Run(() => Worker.Open()).ConfigureAwait(true);
            }
            catch (Exception exception)
            {
                Logger.Log(new LogMessage(exception.Message, true));
            }
        }

        private async void label1_Click(object sender, EventArgs e)
        {
            try
            {
                Cash = await Worker.GetCashAsync();
            }
            catch (Exception exception)
            {
                Logger.Log(new LogMessage(exception.Message, true));
            }
        }

        private async void btnAutoClick_Click(object sender, EventArgs e)
        {
            if (!timer1.Enabled)
            {
                Cash = await Worker.GetCashAsync();
                bet = Worker.Parse(tbxMinBet.Text);
                lblStartCashValue.Text = (await Worker.GetCashAsync()).ToString();
                timer1.Start();
                btnAutoWork.Text = "Зупинити";
            }
            else
            {
                timer1.Stop();
                btnAutoWork.Text = "Запустити";
            }
        }

        private decimal bet;
        private decimal prev;
        private int countOfLose;

        private async void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                prev = Cash.sum;
                Cash = await Worker.GetCashAsync();

                var maxLoseSum = Worker.Parse(tbxMaxLoseSum.Text);
                if (Cash.sum < prev)
                {
                    if (maxLoseSum != 0 && bet >= maxLoseSum)
                    {
                        prev = 0;
                        return;
                    }

                    countOfLose++;
                    if (Coefficients.Length > 0 && countOfLose < Coefficients.Length)
                    {
                        await Worker.SetBetAsync(bet = bet * Coefficients[countOfLose - 1]);
                        lblCoefficientValue.Text = Coefficients[countOfLose - 1].ToString();
                    }
                    else
                    {
                        await Worker.SetBetAsync(bet = bet * 2);
                        lblCoefficientValue.Text = 2.ToString();
                    }

                    IsLeftButton = IsLeftButton;
                }
                else
                {
                    await Worker.SetBetAsync(bet = Worker.Parse(tbxMinBet.Text));
                    IsLeftButton = RandomBool;
                    lblCoefficientValue.Text = 1.ToString();
                    Logger.Log(new LogMessage("Програшів-" + countOfLose));
                    countOfLose = 0;
                }

                lblLoseCountValue.Text = countOfLose.ToString();
            }
            catch (Exception exception)
            {
                Logger.Log(new LogMessage(exception.Message, true));
            }
        }


        private async void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPage.Name != "tabPage2" || !File.Exists(HistoryFileLogger.S_Path)) return;

            var columns = dataGridView1.Columns;
            DataTable table = new DataTable();
            foreach (DataGridViewColumn item in columns)
            {
                table.Columns.Add(item.HeaderText);
            }

            dataGridView1.Columns.Clear();
            dataGridView1.DataSource = await Task.Run(() =>
            {
                Dictionary<int, DataRow> rows = new Dictionary<int, DataRow>();
                foreach (var item in File.ReadAllLines(HistoryFileLogger.S_Path))
                {
                    var count = int.Parse(item.Split('-')[1]);
                    DataRow dr = table.NewRow();
                    dr[0] = count;
                    dr[1] = 1;

                    if (rows.ContainsKey(count))
                    {
                        dr = rows[count];
                        int t = int.Parse(dr[1].ToString());
                        dr[1] = ++t;
                    }
                    else rows.Add(count, dr);
                }

                foreach (DataRow dataRow in rows.Values.OrderBy((x) => int.Parse(x[0].ToString())))
                {
                    table.Rows.Add(dataRow);
                }
                table.AcceptChanges();
                return table;
            });
        }

        private void tbxMinBet_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal d = Worker.Parse(tbxMinBet.Text);
                if (d < 0.00000001M) throw new Exception();
            }
            catch (Exception)
            {
                tbxMinBet.Text = 0.00000001M.ToString().Replace(',', '.');
            }
        }

        private async void tbxСoefficient_Leave(object sender, EventArgs e)
        {
            var coefficients = tbxСoefficient.Text;

        }
    }
}