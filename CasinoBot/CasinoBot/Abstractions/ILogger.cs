﻿using CasinoBot.Models;

namespace CasinoBot.Abstractions
{
    public interface ILogger
    {
        string Path { get; }
        void Log(LogMessage message);
    }
}
