﻿using System.Threading.Tasks;

namespace CasinoBot.Abstractions
{
    public interface IWebWorker
    {
        void Open();

        Task<(decimal sum, string type)> GetCashAsync();
        Task SetBetAsync(decimal sum);
        Task ClickLeftAsync();
        Task ClickRightAsync();

        Task<bool> RollAsync();

        decimal Parse(string value);
        decimal[] ParseCoefficients(string str);
    }
}