﻿using CasinoBot.Abstractions;
using OpenQA.Selenium;
using System;
using System.Globalization;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using Keys = OpenQA.Selenium.Keys;

namespace CasinoBot.Models
{
    public class YobitWorker : IWebWorker
    {
        private IWebDriver _wd;

        private IWebElement _bet;
        private IWebElement _leftBtn;
        private IWebElement _rightBtn;
        private IWebElement _cash;
        private readonly string _login, _pass;

        public YobitWorker(string login, string pass)
        {
            _login = login;
            _pass = pass;
        }

        public void Open()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments(@"%LOCALAPPDATA%\Google\Chrome\User Data");
            _wd = new ChromeDriver(options);
            
            _wd.Navigate().GoToUrl("https://yobit.io/ru/dice/");

            try
            {
                _wd.FindElement(By.ClassName("login")).Click();
                if (string.IsNullOrEmpty(_login) || string.IsNullOrEmpty(_pass)) return;

                _wd.FindElements(By.Name("email"))[1].SendKeys(_login);
                _wd.FindElement(By.Name("psw")).SendKeys(_pass);
            }
            catch
            {
                // ignored
            }
        }

        public async Task<(decimal sum, string type)> GetCashAsync()
        {
            return await Task<(decimal sum, string type)>.Run(()=> {
                _cash = _wd.FindElement(By.CssSelector(".chosen-single span"));

                if (!_cash.Text.Contains(" - "))
                    return (0, " ");

                var pack = _cash.Text.Split('-')[1].TrimStart().Split(' ');
                if (pack.Length != 2)
                    return (0, " ");

                var value = Parse(pack[0]);
                var moneyType = pack[1];
                return (value, moneyType);
            });
        }

        public async Task ClickLeftAsync()
        {
            await Task.Run(()=> {
                _leftBtn = _wd.FindElement(By.XPath("//input[@value='Roll < 48']"));
                _leftBtn.Click();
            });
        }

        public async Task ClickRightAsync()
        {
            await Task.Run(() =>
            {
                _rightBtn = _wd.FindElement(By.XPath("//input[@value='Roll > 52']"));
                _rightBtn.Click();
            });
        }

        public async Task SetBetAsync(decimal sum)
        {
            await Task.Run(() =>
            {
                _bet = _wd.FindElement(By.Name("bet"));

                if (!_bet.Enabled) return;

                for (var i = 0; i < 20; i++)
                {
                    _bet.SendKeys(Keys.Backspace);
                }
                _bet.SendKeys(sum.ToString().Replace(',', '.'));
            });
        }

        public Task<bool> RollAsync()
        {
            throw new System.NotImplementedException();
        }


        public decimal Parse(string str) {
            return decimal.Parse(str.Replace(',','.'), CultureInfo.InvariantCulture);
        }

        public decimal[] ParseCoefficients(string str)
        {
            var coefficients = str.Split(';', ' ');
            var ret = new decimal[coefficients.Length];

            for (int i = 0; i < coefficients.Length; i++)
            {
                ret[i] = Parse(coefficients[i]);
            }

            return ret;
        }
    }
}
