﻿using Autofac;
using CasinoBot_Stable.Configs;

namespace CasinoBot_Stable
{
    public partial class App
    {
        App()
        {
            var container = AutofacConfig.Container;
            container.Resolve<Startup>();
        }
    }
}