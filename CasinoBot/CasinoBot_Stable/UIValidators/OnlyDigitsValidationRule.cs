﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace CasinoBot_Stable.UIValidators
{
    public class OnlyDigitsValidationRule
    {
        public bool Validate(string value)
        {
            if (value != null && !string.IsNullOrEmpty(value))
            {
                return decimal.TryParse(value, out var a);
            }
            return false;
        }
    }
}
