﻿using Autofac;
using CasinoBot_Stable.Configs.Modules;

namespace CasinoBot_Stable.Configs
{
    static class AutofacConfig
    {
        private static IContainer _sContainer;

        public static IContainer Container
        {
            get
            {
                if (_sContainer == null)
                    Configure();

                return _sContainer;
            }
            private set => _sContainer = value;
        }

        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<ViewModule>();
            builder.RegisterModule<LoggersModule>();
            builder.RegisterModule<ModelsModule>();

            _sContainer = builder.Build();
            return _sContainer;
        }
    }
}
