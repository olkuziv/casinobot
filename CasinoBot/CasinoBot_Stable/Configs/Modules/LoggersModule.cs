﻿using Autofac;
using CasinoBot_Stable.Abstractions;
using CasinoBot_Stable.Loggers;

namespace CasinoBot_Stable.Configs.Modules
{
    public class LoggersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var loggers = new ILogger[]
            {
                new ConsoleLogger(), new FileLogger(), new HistoryFileLogger()
            };

            builder
                .RegisterInstance(loggers)
                .As<ILogger[]>()
                .SingleInstance();

            builder
                .RegisterType<LoggerManager>()
                .AsSelf()
                .SingleInstance();
        }
    }
}
