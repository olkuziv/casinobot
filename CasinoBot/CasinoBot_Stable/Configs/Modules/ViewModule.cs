﻿using System.Windows;
using Autofac;
using CasinoBot_Stable.UIValidators;
using CasinoBot_Stable.View;
using CasinoBot_Stable.View.Pages;

namespace CasinoBot_Stable.Configs.Modules
{
    public class ViewModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<Startup>()
                .AsSelf();
            builder
                .RegisterType<OnlyDigitsValidationRule>()
                .AsSelf()
                .SingleInstance();

            builder
                .RegisterType<BotWindow>()
                .As<Window>();

            builder
                .RegisterType<LogsPage>()
                .AsSelf();
            builder
                .RegisterType<MainPage>()
                .AsSelf();
        }
    }
}