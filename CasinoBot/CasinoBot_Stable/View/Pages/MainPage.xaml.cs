﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;
using CasinoBot_Stable.Abstractions;
using CasinoBot_Stable.Loggers;
using CasinoBot_Stable.Models;
using CasinoBot_Stable.UIValidators;

namespace CasinoBot_Stable.View.Pages
{
    public partial class MainPage : INotifyPropertyChanged
    {
        private readonly IWebWorker _worker;
        private readonly LoggerManager _logger;
        private readonly OnlyDigitsValidationRule _digitValidator;
        private readonly DispatcherTimer _timer; 

        public MainPage(IWebWorker worker, LoggerManager logger, OnlyDigitsValidationRule digitValidator)
        {
            _worker = worker;
            _logger = logger;
            _digitValidator = digitValidator;
            _timer = new DispatcherTimer();
            _timer.Tick += timer1_Tick;
            _timer.Interval = new TimeSpan(0, 0, 10);

            InitializeComponent();
            DataContext = this;
        }
        #region UiFields

        /// <summary>
        /// Contains data from ui field
        /// </summary>
        public decimal Cash
        {
            get => _cash;
            set
            {
                _cash = value;
                OnPropertyChanged(nameof(Cash));
            }
        }

        /// <summary>
        /// Contains data from ui field
        /// </summary>
        public int CountOfLose
        {
            get => _countOfLose;
            set
            {
                _countOfLose = value;
                OnPropertyChanged(nameof(CountOfLose));
                OnPropertyChanged(nameof(Coefficient));
            }
        }

        /// <summary>
        /// Contains data from ui field
        /// </summary>
        public decimal Coefficient
        {
            get => _coefficient;
            set
            {
                _coefficient = value;
                OnPropertyChanged(nameof(Coefficient));
                OnPropertyChanged(nameof(Variants));
            }
        }

        /// <summary>
        /// Contains data from ui field
        /// </summary>
        public decimal StartBalance
        {
            get => _startBalance;
            set
            {
                _startBalance = value;
                OnPropertyChanged(nameof(StartBalance));
            }
        }

        /// <summary>
        /// Contains data from ui field in "Bot setting" page
        /// </summary>
        public decimal MinBet
        {
            get => _minBet;
            set
            {
                _minBet = value;
                OnPropertyChanged(nameof(Variants));
            }
        }

        /// <summary>
        /// Contains data from ui field in "Bot setting" page
        /// </summary>
        public decimal MaxBet { get; set; } = 1.00000000M;
        /// <summary>
        /// Contains data from ui field in "Bot setting" page
        /// </summary>
        public decimal[] Coefficients { get; set; } = {5,4,4,3,3};

        public string CoefficientsStr
        {
            get => string.Join(" ", Coefficients);
            set
            {
                Coefficients = _worker.ParseCoefficients(value);
                OnPropertyChanged(nameof(Variants));
            }
        }

        public Dictionary<int, decimal> Variants
        {
            get
            {
                var bet = MinBet;
                var ret = new Dictionary<int, decimal>(Coefficients.Length);

                ret.Add(1, MinBet);

                for (var i = 0; i < Coefficients.Length; i++)
                {
                    ret.Add(i+2, bet = bet * Coefficients[i]);
                }
                for (var i = Coefficients.Length + 2; i < 20; i++)
                {
                    ret.Add(i, bet = bet * 2);
                }
                return ret;
            }
        }

        #endregion

        private bool ClickLeftButton
        {
            get => _clickLeftButton;
            set
            {
                _clickLeftButton = value;
                if (value) _worker.ClickLeftAsync();
                else _worker.ClickRightAsync();
            }
        }

        private decimal Bet {get;set;}
        private bool RandomBool => new Random().Next(100) < 50;

        #region Methods

        #endregion

        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private async void btnOpen_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                await Task.Run(() => _worker.Open()).ConfigureAwait(true);
            }
            catch (Exception exception)
            {
                _logger.Log(new LogMessage(exception.Message, true));
            }
        }

        private async void btnStart_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                if (!_timer.IsEnabled)
                {
                    Cash = await _worker.GetCashAsync();
                    Bet = MinBet;
                    StartBalance = await _worker.GetCashAsync();
                    OnPropertyChanged(nameof(StartBalance));
                    OnPropertyChanged(nameof(Cash));
                    _timer.Start();
                    btnStartWork.Content = "Зупинити";
                }
                else
                {
                    _timer.Stop();
                    btnStartWork.Content = "Запустити";
                }
            }
            catch (Exception exception)
            {
                _logger.Log(new LogMessage(exception.Message, true));
            }
        }

        private decimal _prev;
        private bool _clickLeftButton;
        private decimal _cash = 0.00000001M;
        private int _countOfLose;
        private decimal _coefficient = 3;
        private decimal _startBalance = 0.00000001M;
        private decimal _minBet = 0.00000001M;

        private async void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                _prev = Cash;
                Cash = await _worker.GetCashAsync();

                if (Cash < _prev)
                {
                    if (MaxBet != 0 && Bet >= MaxBet)
                    {
                        _prev = 0;
                        CountOfLose = 0;
                        return;
                    }

                    CountOfLose++;
                    if (Coefficients.Length > 0 && CountOfLose <= Coefficients.Length)
                    {
                        await _worker.SetBetAsync(Bet = Bet * Coefficients[CountOfLose - 1]);
                        Coefficient = Coefficients[CountOfLose - 1];
                    }
                    else
                    {
                        await _worker.SetBetAsync(Bet = Bet * 2);
                        Coefficient = 2M;
                    }

                    ClickLeftButton = ClickLeftButton;
                }
                else
                {
                    await _worker.SetBetAsync(Bet = MinBet);
                    ClickLeftButton = RandomBool;
                    Coefficient = 1;
                    _logger.Log(new LogMessage("Програшів-" + CountOfLose));
                    CountOfLose = 0;
                }
            }
            catch (Exception exception)
            {
                _logger.Log(new LogMessage(exception.Message, true));
            }
        }

        private async void TextBlock_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Cash = await _worker.GetCashAsync();
        }

        private void TextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            btnStartWork.IsEnabled = _digitValidator.Validate(((TextBox) sender).Text);
        }
    }
}