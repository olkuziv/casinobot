﻿using System.ComponentModel;
using System.Windows.Controls;
using CasinoBot_Stable.View.Pages;

namespace CasinoBot_Stable.View
{
    public partial class BotWindow : INotifyPropertyChanged
    {
        private UserControl _currentPage;
        private UserControl[] _pages;

        public BotWindow(MainPage mainPage, LogsPage logsPage)
        {
            Pages = new UserControl[]{ mainPage, logsPage};
            CurrentPage = mainPage;
            InitializeComponent();
            DataContext = this;
        }

        public UserControl[] Pages
        {
            get => _pages;
            set
            {
                _pages = value;
                OnPropertyChanged(nameof(Pages));
            }
        }

        public UserControl CurrentPage
        {
            get => _currentPage;
            set
            {
                _currentPage = value;
                OnPropertyChanged(nameof(CurrentPage));
            }
        }
        #region OnPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}