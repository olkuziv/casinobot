﻿using System;
using CasinoBot_Stable.Abstractions;
using CasinoBot_Stable.Models;

namespace CasinoBot_Stable.Loggers
{
    public class ConsoleLogger : ILogger
    {
        public string Path { get; } = null;

        public void Log(LogMessage message)
        {
            Console.WriteLine(message.Message);
        }
    }
}
