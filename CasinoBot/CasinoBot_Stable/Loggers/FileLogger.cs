﻿using System.IO;
using CasinoBot_Stable.Abstractions;
using CasinoBot_Stable.Models;

namespace CasinoBot_Stable.Loggers
{
    public class FileLogger : ILogger
    {
        public FileLogger()
        {
            if (!Directory.Exists("Logs"))
                Directory.CreateDirectory("Logs");
        }

        public string Path { get; private set; } = "Logs/Log.log";
        public void Log(LogMessage message)
        {
            if(message.IsError)
                File.AppendAllText(Path, message.Message + "\n");
        }
    }
}
