﻿using CasinoBot_Stable.Models;

namespace CasinoBot_Stable.Abstractions
{
    public interface ILogger
    {
        string Path { get; }
        void Log(LogMessage message);
    }
}
