﻿using System.Threading.Tasks;

namespace CasinoBot_Stable.Abstractions
{
    public interface IWebWorker
    {
        void Open();

        Task<decimal> GetCashAsync();
        Task SetBetAsync(decimal sum);
        Task ClickLeftAsync();
        Task ClickRightAsync();

        Task<bool> RollAsync();

        decimal Parse(string value);
        decimal[] ParseCoefficients(string str);
    }
}