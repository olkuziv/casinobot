﻿namespace CasinoBot_Stable.Models
{
    public class LogMessage
    {
        public LogMessage(string message, bool isError = false)
        {
            Message = System.DateTime.Now.ToLocalTime() + " " + message;
            IsError = isError;
        }

        public string Message { get; private set; }
        public bool IsError { get; private set; }
    }
}
