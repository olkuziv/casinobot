﻿using System;
using System.IO;
using CasinoBot.Abstractions;
using CasinoBot.Loggers;
using CasinoBot.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class LoggersTests
    {
        [TestMethod]
        public void LoggerManager_Log_ThrowExceptionIfLoggersIsEmpty()
        {
            var loggers = new ILogger[0];
            var logger = new LoggerManager(loggers);
            Assert.ThrowsException<Exception>(
                () => logger.Log(new LogMessage("Hello logger"))
            );


            logger = new LoggerManager(null);
            Assert.ThrowsException<Exception>(
                () => logger.Log(new LogMessage("Hello logger"))
            );
        }

        [TestMethod]
        public void LoggerManager_HistoryLoger_IsWork()
        {
            var loggers = new ILogger[]
            {
                 new HistoryFileLogger()
            };
            var logger = new LoggerManager(loggers);

            File.Delete(HistoryFileLogger.S_Path); 
            logger.Log(new LogMessage("Hello world tests"));
            Assert.IsTrue(File.Exists(HistoryFileLogger.S_Path));
        }
    }
}
